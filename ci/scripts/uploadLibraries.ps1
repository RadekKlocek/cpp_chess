$librariesPath = Join-Path -Path $PSScriptRoot -ChildPath "..\..\libraries"
$boostZipPath = (Join-Path -Path $librariesPath -ChildPath "boost_1_83_0.zip")
$sfmlZipPath = (Join-Path -Path $librariesPath -ChildPath "SFML-2.5.1-windows-vc15-64-bit.zip")

 
if(Test-Path -Path $librariesPath)
{
    Write-Output "Removing current libraries"
    Remove-Item -Path $librariesPath -Recurse
    Write-Output "Libraries removed"
}

Write-Output "Create libraries directory"
$null = New-Item -Path (Join-Path -Path $librariesPath -ChildPath "\..") -Name "libraries" -ItemType Directory
Write-Output "Libraries directory created"

Write-Output "Download boost 1.83.0"
Invoke-WebRequest -Uri "https://boostorg.jfrog.io/artifactory/main/release/1.83.0/source/boost_1_83_0.zip" -OutFile $boostZipPath
Write-Output "Downloaded boost 1.83.0"

Write-Output "Download sfml 2.5.1"
Invoke-WebRequest -Uri "https://www.sfml-dev.org/files/SFML-2.5.1-windows-vc15-64-bit.zip" -OutFile $sfmlZipPath
Write-Output "Downloaded sfml 2.5.1"

Write-Output "Unzip boost 1.83.0"
Expand-Archive $boostZipPath -Destination $librariesPath
Write-Output "Unziped boost 1.83.0"

Write-Output "Unzip sfml 2.5.1"
Expand-Archive $sfmlZipPath -Destination $librariesPath
Write-Output "Unziped sfml 2.5.1"

Write-Output "Delete boost 1.83.0 zip"
Remove-Item $boostZipPath
Write-Output "Deleted boost 1.83.0 zip"

Write-Output "Delete sfml 2.5.1 zip"
Remove-Item $sfmlZipPath
Write-Output "Deleted sfml 2.5.1 zip"
