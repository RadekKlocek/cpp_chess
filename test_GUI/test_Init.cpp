#include "Gui.h"
#include "Gui_config.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GUI_TESTS
{
	TEST_CLASS(test_Init)
	{

	public:
		std::string configPath = "../../../../../GUI/config.txt";
		TEST_METHOD(InitTest_TestLoadMenuTexture)
		{
			int num_of_menu_graphics = 7;
			st_map_t test_map_textures = {};
			configData test_config_data;
			load_config(test_config_data, configPath);
			load_textures(test_map_textures, test_config_data.graphicPaths, "menu");
			Assert::IsTrue(test_map_textures.size() == num_of_menu_graphics);
		}

		TEST_METHOD(InitTest_TestLoadGameTexture)
		{
			int num_of_game_graphics = 15;
			st_map_t test_map_textures = {};
			configData test_config_data;
			load_config(test_config_data, configPath);
			load_textures(test_map_textures, test_config_data.graphicPaths, "game");
			Assert::IsTrue(test_map_textures.size() == num_of_game_graphics);
		}

		TEST_METHOD(InitTest_TestLoadMenuSprites)
		{
			int num_of_menu_graphics = 7;
			st_map_t test_map_textures = {};
			ssp_map_t test_map_sprites = {};
			configData test_config_data;
			load_config(test_config_data, configPath);
			load_sprites(test_map_sprites, test_map_textures, test_config_data.graphicPaths, "menu", &test_config_data);
			Assert::IsTrue(test_map_sprites.size() == num_of_menu_graphics);
		}

		TEST_METHOD(InitTest_TestLoadGameSprites)
		{
			int num_of_game_graphics = 33;
			st_map_t test_map_textures = {};
			ssp_map_t test_map_sprites = {};
			configData test_config_data;
			load_config(test_config_data, configPath);
			load_sprites(test_map_sprites, test_map_textures, test_config_data.graphicPaths, "game", &test_config_data);
			Assert::IsTrue(test_map_sprites.size() == num_of_game_graphics);
		}

		TEST_METHOD(InitTest_TestLoadWindowSizeFromConfig)
		{
			configData test_cfg_data = { {},{},{} };
			load_config(test_cfg_data, configPath);
			sf::Vector2u disp_size = { 1920, 1080 };
			Assert::IsTrue(test_cfg_data.windowSize.x == disp_size.x);
			Assert::IsTrue(test_cfg_data.windowSize.y == disp_size.y);
		}

		TEST_METHOD(InitTest_TestLoadGraphicPathFromConfig)
		{
			configData test_cfg_data;
			load_config(test_cfg_data, configPath);
			auto graph_path_size = test_cfg_data.graphicPaths.size();
			Assert::IsTrue(test_cfg_data.graphicPaths[0] == "../graphics/menu/Background.png");
			Assert::IsTrue(test_cfg_data.graphicPaths[graph_path_size - 1] == "../graphics/game/white_rook.png");
		}

		TEST_METHOD(InitTest_TestLoadDisplayQueueFromConfig)
		{
			configData test_cfg_data;
			load_config(test_cfg_data, configPath);
			Assert::IsTrue(test_cfg_data.displayQueue.begin()->first == "menu:Background");
			sf::Vector2f first_coords = { 0,0 };
			Assert::IsTrue(test_cfg_data.displayQueue.begin()->second == first_coords);

			Assert::IsTrue((--test_cfg_data.displayQueue.end())->first == "game:white_rook|2");
			sf::Vector2f end_coords = { 1348,889 }; //change it when white_rook will have other coors
			Assert::IsTrue((--test_cfg_data.displayQueue.end())->second == end_coords);
		}
	};
}