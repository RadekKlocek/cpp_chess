#include "CppUnitTest.h"
#include "Gui.h"
#include "Gui_config.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GUI_TESTS
{

	TEST_CLASS(test_GUI)
	{
	public:
		TEST_METHOD(GuiInitTest_BoardTestExist) {
			board test_board;
			
		}

		TEST_METHOD(GuiStateTest_SetState)
		{
			board test_board;
			test_board.setState(states::menu);
			bool statesAreTheSame = (test_board.getState() == states::menu);
			Assert::IsTrue(statesAreTheSame);
		}

		TEST_METHOD(GuiStateTest_CheckInitState)
		{
			board test_board;
			Assert::IsTrue(test_board.getState() == states::menu); //Menu state is init state
		}

		TEST_METHOD(GuiStateTest_ChangeMenuStateToGameState)
		{
			board test_board;
			test_board.changeState(states::game);
			bool statesAreTheSame = (test_board.getState() == states::game);
			Assert::IsTrue(statesAreTheSame);
		}

		TEST_METHOD(GuiStateTest_NotChangeExitStateToOtherState)
		{
			board test_board(states::exit);
			test_board.changeState(states::game);
			bool statesAreNotTheSame = (test_board.getState() == states::game);
			Assert::IsFalse(statesAreNotTheSame);

			test_board.changeState(states::menu);
			statesAreNotTheSame = (test_board.getState() == states::menu);
			Assert::IsFalse(statesAreNotTheSame);
		}

		TEST_METHOD(GuiGraphicsTest_LoadingSpriteToBoardTest)
		{
			int num_of_game_graphics = 33;
			int num_of_menu_graphics = 7;
			board test_board(states::menu);
			Assert::IsTrue(static_cast<int>(test_board.getSizeMenuSprites()) == num_of_menu_graphics);
			Assert::IsTrue(static_cast<int>(test_board.getSizeGameSprites()) == num_of_game_graphics);
		}

		TEST_METHOD(GuiGraphicsTest_LocateSpriteFromBoardOnSpecificPlace)
		{
			board test_board(states::game);
			float test_x = 10;
			float test_y = 10;
			test_board.setObjectPosition("black_pawn|1", test_x, test_y);
			Assert::IsTrue(test_board.getObjectPosition("black_pawn|1").x == test_x);
			Assert::IsTrue(test_board.getObjectPosition("black_pawn|1").y == test_y);
		}

		TEST_METHOD(GuiGraphicsTest_GetObjectScale)
		{
			board test_board(states::menu);
			float test_x = 0.5;
			float test_y = 0.5;
			Assert::IsTrue(test_board.getScale("PlayButton").x == test_x);
			Assert::IsTrue(test_board.getScale("PlayButton").y == test_y);
		}


		TEST_METHOD(GuiGraphicsStateTest_LocateSpriteFromBoardOnSpecificPlaceOnSpecificState)
		{

			board test_board(states::menu);

			short prev_test_x = test_board.getObjectPosition("black_pawn|1").x;
			short prev_test_y = test_board.getObjectPosition("black_pawn|1").y;

			short test_x = 10;
			short test_y = 10;
			test_board.setObjectPosition("black_pawn|1", test_x, test_y);
			Assert::IsTrue(static_cast<short>(test_board.getObjectPosition("black_pawn|1").x) == prev_test_x);
			Assert::IsTrue(static_cast<short>(test_board.getObjectPosition("black_pawn|1").y) == prev_test_y);
		}

		TEST_METHOD(GuiGraphicsStateTest_GetSpriteFromBoardForNewObjects)
		{

			board test_board(states::game);


			sf::Vector2f coords = { 565,210 };
			Assert::IsTrue(test_board.getObjectPosition("black_pawn|1") == coords);

		}

		TEST_METHOD(GuiGraphicsStateTest_FillDisplayQueueForMenuState)
		{
			board board_test;
			int num_of_menu_graphics = 7;
			Assert::IsTrue(static_cast<int>(board_test.getDisplayQueue()->size()) == num_of_menu_graphics);
			auto begin_it = board_test.getDisplayQueue()->begin();
			Assert::IsTrue(*begin_it == std::string("Background"));
			auto end_it = --board_test.getDisplayQueue()->end();
			Assert::IsTrue(*end_it == std::string("QuitButtonHighlight"));
		}

		TEST_METHOD(GuiGraphicsStateTest_FillDisplayQueueForGameState)
		{
			board board_test;
			int num_of_game_graphics = 33;
			board_test.changeState(states::game);
			Assert::IsTrue(static_cast<int>(board_test.getDisplayQueue()->size()) == num_of_game_graphics);
			auto begin_it = board_test.getDisplayQueue()->begin();
			Assert::IsTrue(*begin_it == std::string("chess_board"));
			auto end_it = --board_test.getDisplayQueue()->end();
			Assert::IsTrue(*end_it == std::string("white_rook|2"));
		}
		/*
		TEST_METHOD(GuiDisplayTest_DiplayWindowForMenuState)
		{
			board board_test;
			bool test_status = false;
			test_status = board_test.run();
			Assert::IsTrue(test_status == true);
		}

		TEST_METHOD(GuiDisplayTest_DiplayWindowForGameState)
		{
			board board_test;
			board_test.changeState(states::game);
			bool test_status = board_test.run();
			Assert::IsTrue(test_status == true);
		}
		*/
	};
}

