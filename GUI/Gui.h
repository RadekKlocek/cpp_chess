#pragma once
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

#include <list>
#include "boost/algorithm/string.hpp"

#include "Gui_config.h"


//#TODO Refactor: Replace objectCoords to txt file
extern svu_map_t objectCoords;

enum class states: short
{
	menu = 0,
	game = 1,
	exit = 2,
};

class board
{
public:
	board(states f_state = states::menu, 
		  std::string f_path = "../../../../../GUI/config.txt");
	

	void setState(states f_state) { m_state_machine = f_state; }
	void setObjectPosition(const std::string&, const float, const float);

	states getState() { return m_state_machine; }
	size_t getSizeMenuSprites() { return m_menu_sprite_map.size();  }
	size_t getSizeGameSprites() { return m_game_sprite_map.size();  }
	sf::Vector2f getScale(std::string);
	sf::Vector2f getObjectPosition(const std::string&);
	s_list_t* getDisplayQueue() { return m_current_display_queue;  }

	void initStates();
	void scaleObjects();
	void prepareMenuState();
	void prepareGameState();
	void changeState(states);

	bool run();
	bool display();
	void close();

	~board()
	{

	}


private:
	std::unique_ptr<sf::RenderWindow> m_window;
	states m_state_machine;
	s_vec_t m_graphics_path;
	st_map_t m_menu_texture_map;
	st_map_t m_game_texture_map;
	ssp_map_t m_menu_sprite_map;
	ssp_map_t m_game_sprite_map;
	ssp_map_t * m_current_state_sprite_map;
	svf_list_t m_object_coords;
	s_list_t m_menu_display_queue;
	s_list_t m_game_display_queue;
	s_list_t * m_current_display_queue;


};
