#include "gui.h"

board::board(states f_state, std::string f_path): m_state_machine(f_state)
{
	configData config_data;
	load_config(config_data, f_path);
	m_graphics_path = config_data.graphicPaths;
	m_object_coords = config_data.displayQueue;
	initStates();
	scaleObjects();

	unsigned disp_x = config_data.windowSize.x;
	unsigned disp_y = config_data.windowSize.y;
	m_window = std::unique_ptr<sf::RenderWindow>(new sf::RenderWindow({ disp_x , disp_y }, "TestWindow"));

	load_sprites(m_menu_sprite_map, m_menu_texture_map, m_graphics_path, "menu", &config_data);
	load_sprites(m_game_sprite_map, m_game_texture_map, m_graphics_path, "game", &config_data);


	switch(m_state_machine)
	{
		case states::menu:
			m_current_state_sprite_map = &m_menu_sprite_map;
			prepareMenuState();
			break;
		case states::game:
			m_current_state_sprite_map = &m_game_sprite_map;
			prepareGameState();
			break;
		default:
			m_current_state_sprite_map = nullptr;
			break;
	}
}
void board::initStates()
{
	for (auto coords_it = m_object_coords.begin(); coords_it != m_object_coords.end(); ++coords_it)
	{
		s_vec_t split_string;
		boost::split(split_string, coords_it->first, [](char c) {return c == ':'; });
		if (split_string[0] == "menu")
		{
			m_menu_display_queue.push_back(split_string[1]);
			m_menu_sprite_map[split_string[1]].setPosition(coords_it->second);
		}
		else if(split_string[0] == "game")
		{
			m_game_display_queue.push_back(split_string[1]);
			m_game_sprite_map[split_string[1]].setPosition(coords_it->second);
		}
	}
}

void board::scaleObjects()
{
	auto beg = m_menu_sprite_map.begin();
	auto end = m_menu_sprite_map.end();
	for(auto it = beg; it != end; ++it)
	{
		it->second.setScale(0.5, 0.5);
	}
	m_menu_sprite_map["Background"].setScale(1, 1);

	beg = m_game_sprite_map.begin();
	end = m_game_sprite_map.end();
	for (auto it = beg; it != end; ++it)
	{
		it->second.setScale(0.5, 0.5);
	}
	m_game_sprite_map["chess_board"].setScale(1.44f, 1.44f);
}

sf::Vector2f board::getObjectPosition(const std::string& f_object)
{
	auto it = m_current_state_sprite_map->find(f_object);
	sf::Vector2f ret_val{ -1.0f, -1.0f };
	if (it != m_current_state_sprite_map->end())
		ret_val = (*m_current_state_sprite_map)[f_object].getPosition();
		

	return ret_val;
}

sf::Vector2f board::getScale(std::string f_object)
{
	auto it = m_current_state_sprite_map->find(f_object);
	sf::Vector2f ret_val{ -1.0f, -1.0f };
	if (it != m_current_state_sprite_map->end())
		ret_val = (*m_current_state_sprite_map)[f_object].getScale();


	return ret_val;
}

void board::setObjectPosition(const std::string& f_object, const float f_x, const float f_y)
{
	auto it = m_current_state_sprite_map->find(f_object);
	if (it != m_current_state_sprite_map->end())
		m_game_sprite_map[f_object].setPosition(f_x, f_y);
}

void board::changeState(states f_new_state)
{
	if(m_state_machine == states::menu)
	{
		if (f_new_state != states::menu)
		{
			m_state_machine = f_new_state;
			if(f_new_state == states::game)
				prepareGameState();
		}
	}
	else if (m_state_machine == states::game)
	{
		if (f_new_state != states::game)
		{
			m_state_machine = f_new_state;
			if (f_new_state == states::menu)
				prepareMenuState();
		}
	}
}

void board::prepareMenuState()
{
	m_current_state_sprite_map = &m_menu_sprite_map;
	m_current_display_queue = &m_menu_display_queue;
}

void board::prepareGameState()
{
	m_current_state_sprite_map = &m_game_sprite_map;
	m_current_display_queue = &m_game_display_queue;
}

bool board::display()
{
	bool status = false;
	while (m_window->isOpen())
	{
		m_window->clear();
		sf::Event windowEvent;
		while (m_window->pollEvent(windowEvent))
		{
			if (windowEvent.type == sf::Event::Closed)
			{
				status = not status;
				m_window->close();
			}
		}
		m_window->draw(m_current_state_sprite_map->at(*(m_current_display_queue->begin())));
		for (auto display_it = ++m_current_display_queue->begin(); 
			      display_it != m_current_display_queue->end();
			      ++display_it)
		{
			m_window->draw((*m_current_state_sprite_map)[*(display_it)]);
		}
		m_window->display();
	}
	return status;
}

bool board::run()
{

	bool status = display();
	return status;

}

void board::close()
{
	m_window->close();
}
