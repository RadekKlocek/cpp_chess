#include "Gui.h"



sf::Sprite obj;

void load_textures(st_map_t& f_map_tex, s_vec_t& f_graphics, std::string f_module)
{
	for (auto& graphic : f_graphics)
	{
		std::string path_to_graphic = graphic;
		s_vec_t split_string;

		boost::split(split_string, graphic, [](char c) {return c == '/'; });

		const size_t module_index = split_string.size() - 2;
		const size_t graph_index = split_string.size() - 1;
		if (split_string[module_index] == f_module)
		{
			s_vec_t split_string_graph;
			boost::split(split_string_graph, split_string[graph_index], [](char c) {return c == '.'; });
			std::string graphic_key = split_string_graph[0];
			f_map_tex[graphic_key].loadFromFile(path_to_graphic);
		}
	}
}

void load_sprites(ssp_map_t& f_map_sprites,
	st_map_t& f_map_tex,
	s_vec_t& f_graphics,
	std::string f_module,
	configData* f_cfg_data)
{
	auto disp_queue = f_cfg_data->displayQueue;
	load_textures(f_map_tex, f_graphics, f_module);
	for (auto& disp_el : disp_queue)
	{
		s_vec_t result;

		boost::split(result, disp_el.first, [](char c) {return c == ':'; });

		std::string figure = result[1];


		std::string texture;
		if (figure.find("|") != std::string::npos)
		{
			s_vec_t figure_split;
			boost::split(figure_split, figure, [](char c) {return c == '|'; });
			texture = figure_split[0];
		}
		else
			texture = figure;


		if (result[0] == f_module)
			f_map_sprites[figure].setTexture(f_map_tex[texture]);
	}
}

enum class mark : short
{
	window_display = 0,
	paths = 1,
	display_settings = 2,
	none = 3
};


void check_mark(std::string& f_line, mark& f_mark)
{
	if (f_line == "#WINDOW SIZE")
		f_mark = mark::window_display;
	else if (f_line == "#PATHS")
		f_mark = mark::paths;
	else if (f_line == "#DISPLAY SETTINGS")
		f_mark = mark::display_settings;
}

std::pair<std::string, sf::Vector2f> split_display_queue(std::string& f_line)
{
	std::pair<std::string, sf::Vector2f> ret_val;
	s_vec_t splited_line = {};
	s_vec_t splited_coords = {};
	float x, y;
	std::stringstream str_val;
	boost::split(splited_line, f_line, [](char c) {return c == ' '; });

	ret_val.first = splited_line[0];

	boost::split(splited_coords, splited_line[1], [](char c) {return c == ','; });
	str_val << splited_coords[0]; str_val >> x;
	str_val.clear();
	str_val << splited_coords[1]; str_val >> y;

	ret_val.second = { x,y };

	return ret_val;
}


void load_config(configData& f_config_data, std::string f_path)
{
	mark section_mark = mark::none;

	std::fstream cfg_file(f_path, std::fstream::in);
	std::string single_line;

	while (std::getline(cfg_file, single_line))
	{
		//single_line.erase(single_line.size() - 1);
		if (single_line == "#WINDOW SIZE" or single_line == "#PATHS" or single_line == "#DISPLAY SETTINGS")
		{
			check_mark(single_line, section_mark);
			continue;
		}
		s_vec_t splited_line{};
		unsigned x = 0;
		unsigned y = 0;
		std::stringstream str_val;
		switch (section_mark)
		{
		case mark::window_display:
			boost::split(splited_line, single_line, [](char c) {return c == ','; });
			str_val << splited_line[0]; str_val >> x;
			str_val.clear();
			str_val << splited_line[1]; str_val >> y;
			f_config_data.windowSize = { x, y };
			break;
		case mark::paths:
			f_config_data.graphicPaths.push_back(single_line);
			break;
		case mark::display_settings:
			f_config_data.displayQueue.push_back(split_display_queue(single_line));
			break;
		default:
			break;
		}
	}
}