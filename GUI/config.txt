#WINDOW SIZE
1920,1080
#PATHS
../graphics/menu/Background.png
../graphics/menu/PlayButton.png
../graphics/menu/PlayButtonHighlight.png
../graphics/menu/HelpButton.png
../graphics/menu/HelpButtonHighlight.png
../graphics/menu/QuitButton.png
../graphics/menu/QuitButtonHighlight.png
../graphics/game/chess_board.jpg
../graphics/game/black_bishop.png
../graphics/game/black_king.png
../graphics/game/black_knight_L.png
../graphics/game/black_knight_R.png
../graphics/game/black_pawn.png
../graphics/game/black_queen.png
../graphics/game/black_rook.png
../graphics/game/white_bishop.png
../graphics/game/white_king.png
../graphics/game/white_knight_L.png
../graphics/game/white_knight_R.png
../graphics/game/white_pawn.png
../graphics/game/white_queen.png
../graphics/game/white_rook.png
#DISPLAY SETTINGS
menu:Background 0,0
menu:PlayButton 811,100
menu:PlayButtonHighlight 812,100
menu:HelpButton 801,406
menu:HelpButtonHighlight 803,406
menu:QuitButton 811,713
menu:QuitButtonHighlight 811,713
game:chess_board 490,40
game:black_bishop|1 766,96
game:black_bishop|2 1103,96
game:black_king 1001,101
game:black_knight_L 659,98
game:black_knight_R 1224,98
game:black_pawn|1 565,210
game:black_pawn|2 676,210
game:black_pawn|3 789,210
game:black_pawn|4 901,210
game:black_pawn|5 1014,210
game:black_pawn|6 1126,210
game:black_pawn|7 1238,210
game:black_pawn|8 1351,210
game:black_queen 882,99
game:black_rook|1 560,98
game:black_rook|2 1348,98
game:white_bishop|1 766,887
game:white_bishop|2 1103,887
game:white_king 999,891
game:white_knight_L 659,889
game:white_knight_R 1219,889
game:white_pawn|1 565,777
game:white_pawn|2 676,777
game:white_pawn|3 789,777
game:white_pawn|4 901,777
game:white_pawn|5 1014,777
game:white_pawn|6 1126,777
game:white_pawn|7 1238,777
game:white_pawn|8 1351,777
game:white_queen 884,890
game:white_rook|1 560,889
game:white_rook|2 1348,889