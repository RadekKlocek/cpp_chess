#pragma once

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <map>
#include <utility>
#include <string>
#include <memory>

using st_map_t = std::map<std::string, sf::Texture>;
using ssp_map_t = std::map<std::string, sf::Sprite>;
using svu_map_t = std::map<std::string, sf::Vector2u>;
using svf_list_t = std::list< std::pair<std::string, sf::Vector2f> >;
using s_vec_t = std::vector<std::string>;
using s_list_t = std::list<std::string>;

extern std::string configPath;

struct configData
{
	sf::Vector2u windowSize;
	s_vec_t		 graphicPaths;
	svf_list_t   displayQueue;
};

void load_textures(st_map_t&, s_vec_t&, std::string);
void load_sprites(ssp_map_t&, st_map_t& x, s_vec_t&, std::string, configData*);
void load_config(configData&, std::string);