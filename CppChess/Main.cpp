#include "main.h"
#include <fstream>

int main()
{
	// When you run program from build path, not from Visual Studio - use default path from board class
	// When program is run from VS - root binary path is project path
	// When program is run from binary - root path is binary path
	std::string configPath = "../GUI/config.txt";

	/*std::ofstream configFile;
	configFile.open(configPath, 'r');
	bool isOpen = configFile.is_open();*/


	board Board(states::menu, configPath);

	Board.run();

	return 0;
}


